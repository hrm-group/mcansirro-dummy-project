# Deployment scripts

This project contains deploy scripts for Cansi dummy project. The following software are required to properly deploy components.

* [Minikube](https://kubernetes.io/docs/tasks/tools/install-minikube/) v1.0
* [Helm](https://helm.sh/) v2.13.1
* [Kubernetes Cluster](https://kubernetes.io/it/) v1.11.1
* [Docker](https://www.docker.com/) v.1.13.1
