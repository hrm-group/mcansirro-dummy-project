# Introduction

This module contains all the kubernetes resources that are deployed to run cansi-dummy-project on a k8s cluster.
This chart bootstraps [your-component](your-url) deployment on a [Kubernetes](https://kubernetes.io) cluster using the [Helm](https://helm.sh) package manager.

The module is structured as follow:
```
helm
├── README.md
└── components
    └── cansi-dummy-project
        ├── Chart.yaml
        ├── README.md
        ├── requirements.yaml
        ├── templates
        │   └── module-one
        │       ├── _helpers.tpl
        │       ├── configmap.yaml
        │       ├── deployment.yaml
        │       └── service.yaml
        └── values.yaml
```

- _Chart.yaml_: specifies the component to install;
- _requirements.yaml_: contains the list of external required charts;
- _templates_: is a folder containing one sub-folder per each module (i.e. module-one);
- __helpers.tpl_: golang template containing variables used in other templates;
  > Copy it for every new module
- _configmap.yaml_: configuration that are mounted within the docker container at startup (in this case they are springboot properties);
- _deployment.yaml: k8s deployment resource;
- _service.yaml_: k8s service resource;
- _values.yaml_: configuration used to create k8s resources;
  > Values are stored in alphabetical order per property. 

> Other modules must be added within the templates folder in a subfolder with the module name.

# Configure HELM

```
# install S3 HELM plugin
helm plugin install https://github.com/hypnoglow/helm-s3.git
# add Doxee's repo to helm
helm repo add doxee-charts s3://com.doxee.helm.repository/charts
# update dependencies for the Chart.yaml file
helm dependency update
``` 

## Local Deploy

#### Prerequisites

* [Minikube](https://kubernetes.io/docs/tasks/tools/install-minikube/) v1.0
* [Helm](https://helm.sh/) v2.13.1
* [Kubernetes Cluster](https://kubernetes.io/it/) v1.11.1
* [Docker](https://www.docker.com/) v.1.13.1

#### Installation steps

* Create the following folders  on your computer that will be mounted as NFS on the various pods.
  * `mkdir -p /opt/dp3-nfs-minikube/dp3`

* Start minikube by setting a memory of at least 6gb and 2 cpu and with mount points
  * `minikube start --cpus 2 --memory 6144 --kubernetes-version v1.11.1 --mount true --mount-string /opt/dp3-nfs-minikube:/mnt/doxee`

* Mount the directory created in step 1 on the minikube virtual machine in /mnt/doxee directory
  * `minikube mount /opt/dp3-nfs-minikube:/mnt/doxee &`

* Type the command to connect the docker engine of minikube to the one of the local machine
  * `eval $(minikube docker-env)`


# Chart

## Configuration
Values used to deploy containers are configured within a YAML file, that is passed to the install command.
Each component has its own parameters; further global defaults are specified for common properties and are used if component values are not specified.

You can also override values parameters using the `--set key=value[,key=value]` argument to `helm install`.

In the following table global properties are described:

Parameter | Description |
--- | --- |
`global.actuatorMonitoringLabelValue` | Monitoring label to use in k8s | 
`global.applicationYaml` | Variables exported through configMap. Values are merged with component.applicationYaml with precedence to component ones | 
`global.image.pullPolicy` | k8s default pull policy. |
`global.image.pullSecrets` | Array of k8s default secrets name. |
`global.image.registry` | Default ECR image repository. |

# Components
## Cansi dummy project Module one

_Enter a description here._

The deployed component uses the parameters defined in the **applicationYaml** variable in values.yml file to the Spring boot application. This is to separate the configuration parameters from the running application and allow a more flexible and easy access to them if any change is required.

### Configuration

The following table lists the configurable parameters of the module-one chart and their default values.

Parameter | Description |
--- | --- |
`moduleOne.applicationYaml` | Variables exported through configMap. Values are merged with global.applicationYaml with precedence to component ones | 
`moduleOne.image.pullPolicy` | k8s pull policy. If not specified, **global.image.pullPolicy** will be used |
`moduleOne.image.pullSecrets` | Array of k8s secrets name. If not specified, **global.image.pullSecrets** will be used |
`moduleOne.image.registry` | ECR image repository. If not specified **global.image.registry** will be used |
`moduleOne.javaOpts` | Java options used to start the JVM |
`moduleOne.replicas` | Number of PODs instances started within k8s |
`moduleOne.resources` | POD resources to use. See [official doc](https://kubernetes.io/docs/concepts/configuration/manage-compute-resources-container/) |
`moduleOne.service` | Service configuration. Allowed types are ClusterIP and NodePort. See [official doc](https://kubernetes.io/docs/concepts/services-networking/service/#service-resource) for details |
