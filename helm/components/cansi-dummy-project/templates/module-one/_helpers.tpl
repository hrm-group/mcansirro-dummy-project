{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}

{{- define "moduleOne.name" -}}
{{- printf "module-one" | trunc 63 | trimSuffix "-" -}}
{{- end }}

{{- define "moduleOne.fullname" -}}
{{- printf "%s-%s" .Release.Name (include "moduleOne.name" .) | trunc 63 | trimSuffix "-" -}}
{{- end -}}

Return the proper Component image name
*/}}
{{- define "moduleOne.image" -}}
{{- $registryName := .Values.moduleOne.image.registry | default .Values.global.image.registry -}}
{{- if $registryName }}
   {{- printf "%s/%s/module-one:%s" $registryName .Chart.Name .Chart.Version -}}
{{- else -}}
  {{- printf "%s:%s" .Chart.Name .Chart.Version -}}
{{- end -}}
{{- end -}}

{{/*
Return the proper Docker Image Registry Secret Names
*/}}
{{- define "moduleOne.image.pullSecrets" -}}
{{/*
Helm 2.11 supports the assignment of a value to a variable defined in a different scope,
but Helm 2.9 and 2.10 does not support it, so we need to implement this if-else logic.
Also, we can not use a single if because lazy evaluation is not an option
*/}}
{{- if or .Values.moduleOne.image.pullSecrets .Values.global.image.pullSecrets }}
imagePullSecrets:
    {{- range .Values.moduleOne.image.pullSecrets | default .Values.global.image.pullSecrets }}
  - name: {{ . }}
    {{- end }}
{{- end -}}
{{- end -}}

{{/*
Return Docker Image Pull Policy
*/}}
{{- define "moduleOne.image.pullPolicy" -}}
{{- printf "%s" (default .Values.moduleOne.image.pullPolicy .Values.global.image.pullPolicy) -}}
{{- end -}}

{{/*
Return Globally used application.yaml properties
*/}}

{{- define "moduleOne.tomcatPort" -}}
{{- $yaml := merge dict .Values.moduleOne.applicationYaml .Values.global.applicationYaml -}}
{{- index (index $yaml "server") "port" -}}
{{- end -}}

{{- define "moduleOne.configMapSha" -}}
{{- printf "%s/%s/%s" .Template.BasePath (include "moduleOne.name" .) "configmap.yaml" . | sha256sum -}}
{{- end -}}

{{/*
Grace Period Termination Waiting
*/}}
{{- define "moduleOne.pod.terminationGracePeriodSeconds" -}}
{{- default .Values.global.pod.terminationGracePeriodSeconds .Values.moduleOne.pod.terminationGracePeriodSeconds -}}
{{- end -}}