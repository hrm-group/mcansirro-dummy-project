# Description
This module is made of sub-modules; each of them is a containerized spring-boot application.

## Artifacts
Each sub-module is made of the following artifacts:
- A SpringBoot jar
- A Docker container to run the SpringBoot jar

## SpringBoot
Java code runs as a Spring Boot application and it is configured through an application.yml. The default one is:
```yaml
spring:
  application:
    name: your-module-name
```

### Start SpringBoot
SpringBoot can be run by starting the jar artifact through the java cli:

``java [arguments] -jar [artifact] [artifact-options]``

N.B: In order to properly start SpringBoot, mandatory parameters need to be set at start time by setting properties in the *arguments* section.

#### Set Parameters
You can explicitly set a spring argument by adding the following piece of code in the *arguments* section when you start the jar with java:

``-D{{parameter}}={{value}}``

#### Tips
##### Customize Logging levels
Default logging level are configured in the java-commons-configuration dependency. It is however possible to explicitly set logging level for the different packages as follow:

``java -Dlogging.level.{{java-package}}={{logging-level}}``

##### SpringBoot Parameters in YAML file
Instead of setting explicitly each parameter by command line it is possible to set spring properties by passing to SpringBoot the location to a yaml file to use.

Values within the custom properties file are merged with the defaul properties (see above). In order to achieve this purpose it is sufficient to pass a single argument to java, as follow:
> java -Dspring.config.additional-location={{location-to-application-yaml}} -jar {{jar-location}}

Also logging levels can be set within this file.

## Build/Deploy
This activities are managed through maven. See Bamboo plans in order to inspect mandatory commands to build/deploy. Some information are available in the root README.

