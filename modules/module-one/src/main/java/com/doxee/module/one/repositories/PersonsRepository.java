package com.doxee.module.one.repositories;

import java.util.List;
import java.util.Optional;

import com.doxee.module.one.models.Person;

import org.springframework.data.repository.CrudRepository;

public interface PersonsRepository extends CrudRepository<Person, Long> {

    Optional<Person> findById(Long id);

    List<Person> findByLastName(String lastName);
}
