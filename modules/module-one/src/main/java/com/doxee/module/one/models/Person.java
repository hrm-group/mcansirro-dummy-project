package com.doxee.module.one.models;

import java.util.Objects;

public class Person {

  private Long id;
  private String firstName;
  private String lastName;
  private String birthDate;

  public Long getId() {
    return this.id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getFirstName() {
    return this.firstName;
  }

  public void setFirstName(String firstName) {
    Objects.requireNonNull(firstName);
    if (firstName.trim().isEmpty())
      throw new IllegalArgumentException();
    this.firstName = firstName;
  }

  public String getLastName() {
    return this.lastName;
  }

  public void setLastName(String lastName) {
    Objects.requireNonNull(lastName);
    if (lastName.trim().isEmpty())
      throw new IllegalArgumentException();
    this.lastName = lastName;
  }

  public String getBirthDate() {
    return this.birthDate;
  }

  public void setBirthDate(String birthDate) {
    Objects.requireNonNull(birthDate);
    if (birthDate.trim().isEmpty())
      throw new IllegalArgumentException();
    this.birthDate = birthDate;
  }

  @Override
  public String toString() {
    return this.firstName + " " + this.lastName;
  }
}
