package com.doxee.module.one.web;

import lombok.extern.slf4j.Slf4j;

import com.doxee.module.one.models.Person;
import com.doxee.module.one.repositories.PersonsRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class TestController {

  @Autowired
  private PersonsRepository personsRepository;

  @GetMapping("/test")
  public String greeting() {
    return "Hello World!";
  }

  @GetMapping("/save")
  public String save() {
    Person aPerson = new Person();
    aPerson.setFirstName("Mattia");
    aPerson.setLastName("Cansirro");
    aPerson.setBirthDate("1996-03-02");
    personsRepository.save(aPerson);
    return "OK!";
  }

  @GetMapping("/get")
  public void get() {
    var result = personsRepository.findByLastName("Cansirro");
  }

}
