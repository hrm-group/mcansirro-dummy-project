package com.doxee.module.one;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/*
 * Main class application of Inbound Simulator.
 *
 * Bugs: none known
 *
 * @author mcansirro <mcansirro@doxee.com>
 * 
 * @createDate 2021-01-15
 *
 * Copyright (C) 2019 Doxee S.p.A. C.F. - P.IVA: IT02714390362. All Rights Reserved
 */
@SpringBootApplication
public class Application {
  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }
}
