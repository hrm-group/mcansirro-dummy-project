# Module one
_Enter a description here._

Further information follows in the paragraphs below.

## SpringBoot
Java code runs as a Spring Boot application and it is configured through an application.yml. The default one is:
```yaml
debug: true

spring:
  application:
    name: module-one
  lifecycle:
    timeout-per-shutdown-phase: 310s

server:
  shutdown: graceful  #graceful

management:
  endpoint:
    health:
      show-details: always
  endpoints:
    web:
      base_path: /
      exposure:
        include: "health,prometheus"
  metrics:
    tags:
      application: "${spring.application.name}"
      service: cansi-dummy-project

logging:
  level:
    com:
      doxee: debug
    root: info
```

Within the default configuration file mandatory parameters, used to start spring boot are missed and it is necessary to set them before running the jar.

> In the table below, values with None are mandatory and need to be set.

| Parameter Name | Parameter Description | Default |
| --- | --- | --- | 
| Insert parameters |

### Input
_Enter a description here._

### Output
_Enter a description here._

# Graceful shutdown

It is desirable to properly shutdown the application context. This can be enabled by configuring `server.shutdown` property with value `shutdown` but additional steps are required in order to finish/stop pending tasks (e.g. HTTP requests, scheduled tasks, queue message processing).
Please see [detailed Graceful application shutdown documentation](https://code.doxee.com/confluence/display/DP3/Graceful+Application+Shutdown)

## Relevant Spring Kafka properties involved in application shutdown

Please make sure that time needed to complete poll() `max.poll.interval.ms` is within `spring.lifecycle.timeout-per-shutdown-phase`

```yaml
spring:
  lifecycle:
    timeout-per-shutdown-phase: 310s
  kafka:
    consumer:
      properties:
        max.poll.interval.ms: 300000  #graceful
```

## Relevant Servlet Container properties involved in application shutdown

No additional configuration required. 
Spring Boot provides SmartLifecycle beans that will stop serving new requests and allow pending requests to finish within `spring.lifecycle.timeout-per-shutdown-phase`

## Relevant Scheduled, Programmatic Sync/Async TaskExecutor/s properties involved in application shutdown

The SmartLifecycle bean `com.doxee.kubernetes.support.components.ThreadPoolTaskHolder` tries to gracefully stop in parallel ThreadPoolTaskExecutor and ThreadPoolTaskScheduler executors. 
Please make sure that `task.[execution|scheduling].shutdown.await-termination` is true`and that task.[execution|scheduling].shutdown.await-termination-period` is within `spring.lifecycle.timeout-per-shutdown-phase`

```yaml
spring:
  lifecycle:
    timeout-per-shutdown-phase: 310s
  task:
    execution: # Those properties affect @Async method executions or programmatic task enqueueing with default ThreadPoolTaskExecutor
      shutdown:
        await-termination: true
        await-termination-period: 300s
      pool:
        core-size: 8
        max-size: 8
        queue-capacity: 0
    scheduling: # Those properties affect @Scheduled tasks
      shutdown:
        await-termination-period: 300s      
        await-termination: true            
```




