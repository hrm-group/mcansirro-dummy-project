# Description
This repository folder structure is organized in subfolders with the following structure:

```
|-- cansi-dummy-project
    |-- commons
    |-- modules
    |-- helm
    |-- bamboo-specs
```    

## Prerequisites

* [Java Corretto 11](https://aws.amazon.com/corretto/?nc1=h_ls)
* [Maven 3.6](https://maven.apache.org/download.cgi)

Check your project satisfies all the Doxee's official guidelines available [here](https://code.doxee.com/confluence/display/DP3/Guidelines)

## Commons
This is a project module. It contains code and resources shared against multiple modules within this project. It must contain all the assets that could, potentially, be used in multiple places in orer to avoid code duplication.

## Modules
This is a project module. It is structured in sub-modules, which implement the core logic of a piece of the whole component.

### Add a Module
In order to insert a new module in your components follow the steps below:
1. Create a new java module within _modules_ folder;
2. Insert in the section **modules** in the pom.xml the new module;
3. Add the image in the file _images.json_ in the root folder;
4. Add helm chart; 

## Helm
It is not a project module. It is a simple folder and contains assets necessary to deploy modules on a Kubernetes cluster.
In order to deploy modules Helm is used. This folder contains helm charts; for an insight on helm see the official documentation (linked at the beginning of this document). 

## Bamboo Specs
CI/CD pipeline si managed through Bamboo. Bamboo's build and deploy plans are managed through Bamboo Specs in order to handle manageability.

> If you want to set environments using an external file you should set *deployPlan.environmentsSource* in ./bamboo-specs/src/main/resources/config/properties.yaml. 
> It is a file accessible through HTTP or a shared filesystem.

See the following repositories for an insight:
- https://code.doxee.com/bitbucket/projects/BAM/repos/bamboo-specs-commons/browse

Changes to bamboo-specs are applied only when they are committend on the branch configured as linked repository on Bamboo. Otherwise it is necessary to manually update the Plan using the command in the next section.
In order to check the linked repository branch:
 1. Go to Bamboo build page;
 2. Click on the gear in the top right corner;
 3. Select _Linked repositories_;
 4. Select your repository;
 5. On **Bamboo Specs** tab verify that bamboo specs are enabled;
 6. On **General** tab check the configured branch; 
 

# Commands
In the following subsection, useful commands are described.

### Compile Source Code
To compile the code you need to execute the following steps

In case this is the first time you build on local machine you need to login to ECR repository in order to download base images
  ```bash
 # AWS-CLI 1.0: 
 aws ecr get-login --region eu-central-1 --no-include-email
 # AWS-CLI 2.0: 
 aws ecr get-login-password --region eu-central-1 | docker login --username AWS --password -stdin 034135346153.dkr.ecr.eu-central-1.amazonaws.com
  ```
Then execute following commands: 
 * `mvn clean package` : Compile sources
 * `mvn dockerfile:build` : Create the docker images
 * `mvn sonar:sonar -Dsonar.java.binaries=.` : Init sonarqube project on localhost:9000
 * `mvn clean org.jacoco:jacoco-maven-plugin:0.8.2:prepare-agent test org.sonarsource.scanner.maven:sonar-maven-plugin:3.2:sonar`: Update sonarqube test coverage on localhost:9000


### Build Docker Images
You can build docker images and push them to: 
- local docker registry 
- minikube docker registry
- Doxee ECR docker registry

In order to push images do:
1. Set environment variables:
    ```bash
    export DOCKER_REPO=cansi-dummy-project
    export DOCKER_REGISTRY="034135346153.dkr.ecr.eu-central-1.amazonaws.com"
    export DOCKER_TLS_VERIFY=0
    ```
2. Build the images and push to the local docker registry

    `mvn dockerfile:build -Ddocker.image.prefix=${DOCKER_REGISTRY}/${DOCKER_REPO} -Dmaven.test
    .skip=true -DbuildVersion=1.0.0`
3. You can push images to ECR as follow:
    1. Configure aws-cli with valid credentials;
    2. Login to ECR by executing:
        > AWS-CLI 1.0: `aws ecr get-login --region eu-central-1 --no-include-email`
        >
        > AWS-CLI 2.0: `aws ecr get-login-password --region eu-central-1 | docker login --username AWS --password-stdin 034135346153.dkr.ecr.eu-central-1.amazonaws.com`
    3. Execute the command printed out;
    4. Check if the remote repository you want to push exists and eventually create it;
    5. Then execute the following command:
    
        `mvn dockerfile:push -Ddocker.image.prefix=${DOCKER_REGISTRY}/${DOCKER_REPO} -Dmaven.test
        .skip=true -DbuildVersion=1.0.0`

### Deploy Bamboo Specs
Once you have set Linked repository correctly and enabled bamboo specs, you can deploy specs using the commands below:

```bash
cd bamboo-specs
mvn com.atlassian.bamboo:bamboo-specs-runner:7.0.6:run -U -DbuildVersion=1.0.0
```

### Deploy Helm Charts
If you want to deploy Helm Charts you first need to ensure that images are pushed to the docker repository.
You can follow the paragraph [Deploy Docker Images](#deploy-docker-images).

If you want to deploy helm charts on a Minikube cluster, you can:
1. Build source code;
   
2. Set your local docker registry to minikube one using the command:
    ```bash
    mvn clean install -DbuildVersion=1.0.0
    ```
    `eval $(minikube docker-env)`
    > To make this feature work it is necessary to set deployments PullPolicy to _Never_ or _IfNotPresent_
3. Build the images
    ```bash
    eval $(minikube docker-env)
    export DOCKER_TLS_VERIFY=0
    mvn dockerfile:build -Dmaven.test.skip=true -DbuildVersion=1.0.0   
    ```
   If you use minikube profiles replace the eval command with
   ```bash
   # eval $(minikube -p <profile-name> docker-env) 
   # example:
   eval $(minikube -p profileA docker-env)
   export DOCKER_TLS_VERIFY=0
   mvn dockerfile:build -Dmaven.test.skip=true -DbuildVersion=1.0.0
   ```
4. To install the chart :
   
   ```bash
   helm upgrade --install --namespace default --values helm/clusters/minikube.yaml cansi-dummy-project helm/components/cansi-dummy-project
   ```
### Remove Helm Charts
```bash
# if using helm3
helm delete cansi-dummy-project

# if using helm2
helm delete --purge cansi-dummy-project
```