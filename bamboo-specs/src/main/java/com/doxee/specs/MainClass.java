package com.doxee.specs;

import com.atlassian.bamboo.specs.api.BambooSpec;
import com.doxee.specs.build.plans.DoxeeBuildPlan;
import com.doxee.specs.build.plans.DoxeeComponentBuildHelmV3;
import com.doxee.specs.deploy.plans.CloudComponentEnvironmentHelmV3Support;
import com.doxee.specs.deploy.plans.DoxeeEnvironment;
import com.doxee.specs.deploy.plans.TenantComponentEnvironment;
import com.doxee.specs.deploy.plans.TenantComponentEnvironmentHelmV3Support;
import com.doxee.specs.utils.FileReader;
import com.doxee.specs.vo.BambooVariables;
import com.doxee.specs.vo.EnvironmentConfiguration;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import lombok.SneakyThrows;
import com.doxee.specs.deploy.plans.EKSEnvironment;
import org.apache.commons.lang3.StringUtils;


/*
 * Insert a description here.
 *
 * Bugs: none known
 *
 * @author  gmiano gmiano@doxee.com
 * @createDate  2020-04-06
 *
 * Copyright (C) 2019 Doxee S.p.A. C.F. - P.IVA: IT02714390362. All Rights Reserved
 */
@BambooSpec
public class MainClass {

  @SneakyThrows
  public static void main(String... argv) {
    //By default credentials are read from the '.credentials' file.
    URL properties = MainClass.class.getClassLoader().getResource("config/properties.yaml");
    if ((properties != null ? properties.getFile() : null) == null) {
      throw new IOException("Cannot read properties file");
    }
    BambooVariables variables = FileReader.loadVariables(properties.getFile(), false);

    DoxeeBuildPlan buildPlan = new DoxeeComponentBuildHelmV3(variables.getBuildPlan(),
        variables.getHelm(), variables.getDocker());
    PlanSpec.createPlan(variables, buildPlan.getStages(), buildPlan.getBuildVariables());

    List<DoxeeEnvironment> environments = new ArrayList<>();
    for (EnvironmentConfiguration environmentConfiguration : variables.getDeployPlan()
        .getEnvironments().getRemote()) {
      TenantComponentEnvironment environment = new TenantComponentEnvironmentHelmV3Support(
          variables.getHelm(),
          environmentConfiguration);
      environments.add(environment);
    }

    for (EnvironmentConfiguration environmentConfiguration : variables.getDeployPlan()
        .getEnvironments().getBambooAgent()) {
      DoxeeEnvironment environment;
      if (environmentConfiguration.getScope().equals("dev") && StringUtils
          .isNotEmpty(environmentConfiguration.getVariables().getKubeconfigBucket())) {
        environment = new EKSEnvironment(
            environmentConfiguration,
            variables.getDeployPlan(),
            variables.getHelm()
        );
      } else {
        environment = new CloudComponentEnvironmentHelmV3Support(
            environmentConfiguration,
            variables.getDeployPlan(),
            variables.getHelm()
        );
      }
      environments.add(environment);
    }
    PlanSpec.createDeployment(variables, environments);
  }
}